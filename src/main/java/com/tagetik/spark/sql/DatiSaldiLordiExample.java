package com.tagetik.spark.sql;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static com.tagetik.spark.sql.TgkConnection.*;
import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.storage.StorageLevel.MEMORY_ONLY;

public class DatiSaldiLordiExample {

    private static final String APP_NAME = "Dati Saldi Lordi JDBC";
    private SparkSession spark;

    protected DatiSaldiLordiExample(SparkSession spark) {
        this.spark = spark;
    }

    public static DatiSaldiLordiExample create() {
        SparkSession spark = SparkSession
                .builder()
                .appName(APP_NAME)
                .getOrCreate();
        return new DatiSaldiLordiExample(spark);
    }

    public Row getFirstRow() {
        Dataset<Row> datiSaldiLordi = getDatiSaldiLordiDataset();
        datiSaldiLordi.createOrReplaceTempView("DATI_SALDI_LORDI");
        System.out.println("Number of record: " + datiSaldiLordi.count());
        long startTime = System.currentTimeMillis();
        Column codAzienda = datiSaldiLordi.col("COD_AZIENDA");
        Row first = datiSaldiLordi.select("COD_AZIENDA", "IMPORTO")
                    .groupBy(codAzienda)
                    .agg(codAzienda, avg("IMPORTO"))
                    .orderBy(codAzienda)
                    .first();
        //Row first = spark.sql("SELECT COD_AZIENDA, AVG(IMPORTO) FROM DATI_SALDI_LORDI GROUP BY COD_AZIENDA ORDER BY COD_AZIENDA").first();
        long endTime = System.currentTimeMillis() - startTime;
        System.out.println("Query execution time: " + endTime + " ms");
        return first;
    }

    private Dataset<Row> getDatiSaldiLordiDataset() {
        return spark.read().format("jdbc")
                .option("url", SQL_CONNECTION_URL)
                .option("driver", SQL_DRIVER)
                .option("user", SQL_USERNAME)
                .option("password", SQL_PWD)
                .option("dbtable", "DATI_SALDI_LORDI")
                .load()
                .persist(MEMORY_ONLY());
    }

    public static void main(String[] args) {
        Row row = create().getFirstRow();
        System.out.println(row.toString());
    }

}
