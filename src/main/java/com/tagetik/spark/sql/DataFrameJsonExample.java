package com.tagetik.spark.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class DataFrameJsonExample {

    private static final String APP_NAME = "Show information for Json file";
    private SparkSession spark;
    private String data;

    protected DataFrameJsonExample(SparkSession spark, String data){
        this.spark = spark;
        this.data = data;
    }

    public static DataFrameJsonExample create(String data){
        SparkSession spark = SparkSession
                .builder()
                .appName(APP_NAME)
                .getOrCreate();
        return new DataFrameJsonExample(spark, data);
    }

    public void printSomeInformation(){

        Dataset<Row> df = spark.read().json(this.data);
        df.show();

        // Select only the "name" column
        df.select(col("name"));
        // Select everybody, but increment the age by 1
        df.select(col("name"), col("age").plus(1)).show();
        // Select people older than 21
        df.filter(col("age").gt(21)).show();
        // Count people by age
        df.groupBy("age").count().show();
    }

    public static void main(String[] args) {
        checkInputArgs(args);
        create(args[0]).printSomeInformation();
    }

    private static void checkInputArgs(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: <data>");
            System.exit(1);
        }
    }
}
