package com.tagetik.spark.sql;

public class MySQLConnection {

    public static final String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String MYSQL_USERNAME = "root";
    public static final String MYSQL_PWD = "root";
    public static final String MYSQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/apache-spark-examples";
}
