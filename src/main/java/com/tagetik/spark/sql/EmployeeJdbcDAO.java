package com.tagetik.spark.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static com.tagetik.spark.sql.MySQLConnection.*;

public class EmployeeJdbcDAO {

    private static final String APP_NAME = "Show Employee name via JDBC";
    private SparkSession spark;

    protected EmployeeJdbcDAO(SparkSession spark) {
        this.spark = spark;
    }

    public static EmployeeJdbcDAO createEmployeeDao() {
        SparkSession spark = SparkSession
                .builder()
                .appName(APP_NAME)
                .getOrCreate();
        return new EmployeeJdbcDAO(spark);
    }

    public Dataset<Row> getEmployeeName(){

        Dataset<Row> employeeData = getEmployeeDataset();
        employeeData.createOrReplaceTempView("Employee");
        return spark.sql("SELECT NAME FROM Employee");
    }

    private Dataset<Row> getEmployeeDataset() {
        return spark.read()
                .format("jdbc")
                .option("url", MYSQL_CONNECTION_URL)
                .option("driver", MYSQL_DRIVER)
                .option("user", MYSQL_USERNAME)
                .option("password", MYSQL_PWD)
                .option("dbtable", "Employee")
                .load();
    }

    public static void main(String[] args) {
        createEmployeeDao().getEmployeeName().show();
    }
}
