package com.tagetik.spark.classification;

import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;
import org.apache.spark.mllib.stat.Statistics;
import org.apache.spark.storage.StorageLevel;

public class EvaluateStatistics {

    private static final String APP_NAME = "Basic statistics";
    private JavaSparkContext jsc;
    private String csvFile;

    protected EvaluateStatistics(JavaSparkContext jsc, String csvFile){
        this.jsc = jsc;
        this.csvFile = csvFile;
    }

    public static EvaluateStatistics create(String csvFile){
        SparkConf conf = new SparkConf().setAppName(APP_NAME);
        JavaSparkContext jsc = new JavaSparkContext(conf);
        return new EvaluateStatistics(jsc, csvFile);
    }

    public MultivariateStatisticalSummary getStatistics(){
        long startTime = System.currentTimeMillis();
        JavaRDD<Vector> value = jsc.textFile(csvFile)
                .map(e -> getVector(e))
                .persist(StorageLevel.MEMORY_ONLY());
        System.out.println("Number of records: " + value.count());
        MultivariateStatisticalSummary summary = Statistics.colStats(value.rdd());
        long endTime = System.currentTimeMillis() - startTime;
        System.out.println("Execution time: " + endTime);
        return summary;
    }

    private static Vector getVector(String line) {
        return Vectors.dense(Arrays.stream(line.split(",")).mapToDouble(elem -> Double.parseDouble(elem)).toArray());
    }

    public static void main(String[] args){
        checkInputArgs(args);
        MultivariateStatisticalSummary statistics = create(args[0]).getStatistics();
        System.out.println("max: " + statistics.max());
        System.out.println("min: " + statistics.min());
        System.out.println("mean: " + statistics.mean());
    }

    private static void checkInputArgs(String[] args) {
        if(args.length < 1){
            System.out.println("Usage: <csv path>");
            System.exit(1);
        }
    }

}
