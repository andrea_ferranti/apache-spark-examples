package com.tagetik.spark.rdd;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class CountLineLength {

    private static final String APP_NAME = "Count line length Examples";
    private final JavaSparkContext jsc;
    private String data;

    protected CountLineLength(JavaSparkContext jsc, String data) {
        this.jsc = jsc;
        this.data = data;
    }

    public static CountLineLength create(String data) {
        SparkConf conf = new SparkConf().setAppName(APP_NAME);
        JavaSparkContext jsc = new JavaSparkContext(conf);
        return new CountLineLength(jsc, data);
    }

    public long countLineLength() {
        JavaRDD<String> lines = jsc.textFile(data);
        JavaRDD<Integer> lineLengths = lines.map(s -> s.length());
        return lineLengths.reduce((a, b) -> a + b);
    }

    public static void main(String[] args) {
        checkInputArgs(args);
        long wordNumber = create(args[0]).countLineLength();
        System.out.println("Word Number: " + wordNumber);
    }

    private static void checkInputArgs(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: <data>");
            System.exit(1);
        }
    }
}
