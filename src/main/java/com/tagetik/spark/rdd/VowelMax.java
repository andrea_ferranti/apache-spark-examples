package com.tagetik.spark.rdd;

import java.util.Comparator;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Serializable;
import scala.Tuple2;
import static java.lang.Character.toLowerCase;

public class VowelMax {

    private static final String APP_NAME = "Find most used vowel Example";
    private transient final JavaSparkContext jsc;
    private transient String data;

    protected VowelMax(JavaSparkContext jsc, String data) {
        this.jsc = jsc;
        this.data = data;
    }

    public static VowelMax create(String data) {
        SparkConf conf = new SparkConf().setAppName(APP_NAME);
        JavaSparkContext jsc = new JavaSparkContext(conf);
        return new VowelMax(jsc, data);
    }

    public Tuple2<Character, Long> findMostUsedVowel() {
        JavaRDD<String> lines = jsc.textFile(data);
        JavaRDD<Character> vowels = lines
                .flatMap(line -> line.chars().mapToObj(c -> toLowerCase((char) c)).iterator())
                .filter(VowelMax::isVowel);
        JavaPairRDD<Character, Long> charsAndOccurrences = vowels
                .mapToPair(c -> new Tuple2<>(c, 1L))
                .reduceByKey((c1, c2) -> c1 + c2);
        return charsAndOccurrences.max(new charsAndOccurrencesComparator());
    }

    private static boolean isVowel(char c) {
        return "aeiou".indexOf(c) != -1;
    }

    public static void main(String[] args) {
        checkInputArgs(args);
        Tuple2<Character, Long> vowelAndCount = create(args[0]).findMostUsedVowel();
        System.out.printf("Most used vowel is '%s' (count: %d)%n", vowelAndCount._1, vowelAndCount._2);
    }

    private static void checkInputArgs(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: <data>");
            System.exit(1);
        }
    }

    private static class charsAndOccurrencesComparator implements Comparator<Tuple2<Character, Long>>, Serializable {
        @Override
        public int compare(Tuple2<Character, Long> t1, Tuple2<Character, Long> t2) {
            return t1._2.compareTo(t2._2);
        }
    }
}
