package com.tagetik.spark.rdd;

import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.util.DoubleAccumulator;

public class Scalability {

    private static final String APP_NAME = "Scalability Test";
    private final JavaSparkContext jsc;
    private final String data;

    public Scalability(JavaSparkContext jsc, String data) {
        this.jsc = jsc;
        this.data = data;
    }

    public void execute(){
        DoubleAccumulator count = jsc.sc().doubleAccumulator();
        JavaRDD<double[]> values = jsc.textFile(data).map(e -> stringToVector(e));
        long startTime = System.currentTimeMillis();
        scanDataset(values, count);
        long endTime = System.currentTimeMillis() - startTime;
        System.out.println("Execution time not in memory: " + endTime);
        values.persist(StorageLevel.MEMORY_ONLY()).count();
        count.reset();
        startTime = System.currentTimeMillis();
        scanDataset(values, count);
        endTime = System.currentTimeMillis() - startTime;
        System.out.println("Execution in memory: " + endTime);
        startTime = System.currentTimeMillis();
        for(int i = 0; i < 10; i++){
            count.reset();
            scanDataset(values, count);
        }
        endTime = System.currentTimeMillis() - startTime;
        System.out.println("Execution in memory 10 evaluations: " + endTime);

    }

    private static void scanDataset(JavaRDD<double[]> values, DoubleAccumulator count) {
        values.foreach(elem -> count.add(elem[0]));
    }

    private static double[] stringToVector(String e) {
        return Arrays.stream(e.split(",")).mapToDouble(Double::parseDouble).toArray();
    }

    public static Scalability create(String data){
        SparkConf conf = new SparkConf().setAppName(APP_NAME);
        JavaSparkContext jsc = new JavaSparkContext(conf);
        return new Scalability(jsc, data);
    }

    public static void main(String[] args){
        checkInputArgs(args);
        create(args[0]).execute();
    }


    private static void checkInputArgs(String[] args) {
        if(args.length < 1){
            System.out.println("Usage: <data>");
            System.exit(1);
        }
    }
}
