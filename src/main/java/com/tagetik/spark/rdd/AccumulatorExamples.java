package com.tagetik.spark.rdd;

import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.util.LongAccumulator;

public class AccumulatorExamples {

    private static final String APP_NAME = "Accumulator Examples";
    private final JavaSparkContext jsc;
    private String data;

    protected AccumulatorExamples(JavaSparkContext jsc, String data){
        this.jsc = jsc;
        this.data = data;
    }

    public static AccumulatorExamples create(String data){
        SparkConf conf = new SparkConf().setAppName(APP_NAME);
        JavaSparkContext jsc = new JavaSparkContext(conf);
        return new AccumulatorExamples(jsc, data);
    }

    public long countLineLength(){
        LongAccumulator numberOfWords = jsc.sc().longAccumulator();
        JavaRDD<String> lines = jsc.textFile(data);
        lines.foreach(s -> numberOfWords.add(s.length()));
        return numberOfWords.value();
    }

    public long wordCount(){
        JavaRDD<String> lines = jsc.textFile(data);
        JavaRDD<String> words = lines.flatMap(s -> Arrays.asList(s.split(" ")).iterator());
        LongAccumulator numberOfWords = jsc.sc().longAccumulator();
        words.foreach(word -> numberOfWords.add(1));
        return numberOfWords.value();
    }

    public static void main(String[] args){
        checkInputArgs(args);
        long wordNumber = create(args[0]).countLineLength();
        System.out.println("Word Number: " + wordNumber);
    }

    private static void checkInputArgs(String[] args) {
        if(args.length < 1){
            System.out.println("Usage: <data>");
            System.exit(1);
        }
    }
}
