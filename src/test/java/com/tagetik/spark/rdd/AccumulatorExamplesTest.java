package com.tagetik.spark.rdd;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccumulatorExamplesTest {

    private final static String APP_NAME_TEST = "Count line length Examples Test";
    private AccumulatorExamples sut;
    private JavaSparkContext jsc;

    @Before
    public void setUp() {
        SparkConf conf = new SparkConf().setAppName(APP_NAME_TEST).setMaster("local[*]");
        jsc = new JavaSparkContext(conf);
        sut = new AccumulatorExamples(jsc, "src/test/resources/data.txt");
    }

    @After
    public void tearDown() {
        jsc.stop();
    }

    @Test
    public void testCountLineLength() {
        assertEquals(2729, sut.countLineLength());
    }

    @Test
    public void testWordCount() {
        assertEquals(418, sut.wordCount());
    }
}
