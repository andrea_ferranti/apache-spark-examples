package com.tagetik.spark.rdd;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import scala.Tuple2;
import static org.junit.Assert.assertEquals;

public class VowelMaxTest {

    private static final String APP_NAME_TEST = "Find most used vowel Test";
    private VowelMax sut;
    private JavaSparkContext jsc;

    @Before
    public void setUp(){
        SparkConf conf = new SparkConf().setAppName(APP_NAME_TEST).setMaster("local[*]");
        jsc = new JavaSparkContext(conf);
        sut = new VowelMax(jsc, "src/test/resources/dante.txt");
    }

    @After
    public void tearDown(){
        jsc.stop();
    }

    @Test
    public void testCountLineLength(){
        assertEquals(new Tuple2<>('e', 48827L), sut.findMostUsedVowel());
    }
}
