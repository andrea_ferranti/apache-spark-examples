package com.tagetik.spark.classification;

import org.apache.spark.sql.SparkSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class DecisionTreeTest {

    private static final String APP_NAME = "Decision Tree classifier example test";
    private SparkSession spark;
    private DecisionTree tree;

    @Before
    public void setUp(){
        this.spark = SparkSession.builder()
                        .appName(APP_NAME)
                        .master("local[*]")
                        .getOrCreate();
        this.tree = new DecisionTree(spark, "src/test/resources/sample_libsvm_data.txt", 4);
    }

    @After
    public void tearDown(){
        this.spark.stop();
    }

    @Test
    @Ignore
    public void doClassification() throws Exception {
        tree.doClassification();
    }
}