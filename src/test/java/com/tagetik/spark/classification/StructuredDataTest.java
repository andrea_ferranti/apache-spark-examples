package com.tagetik.spark.classification;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Ignore;
import org.junit.Test;

public class StructuredDataTest {

    @Test
    @Ignore
    public void test(){
        SparkSession spark = SparkSession.builder().master("local[*]").appName("test").getOrCreate();
        //    StructField[] fields = new StructField[]{new StructField("label", new StringType(), false, Metadata.empty())};
        StructField[] fields = new StructField[1];
        fields[0] = DataTypes.createStructField("label", DataTypes.StringType, false);
        StructType schema = DataTypes.createStructType(fields);
        // Load the dataset stored in CSV format as a DataFrame.
        Dataset<Row> csv = spark.read().schema(schema).option("header", false).csv("src/test/resources/HIGGS_Test.csv");
        csv.show();
        spark.stop();
    }
}
