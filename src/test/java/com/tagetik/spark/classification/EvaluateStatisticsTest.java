package com.tagetik.spark.classification;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class EvaluateStatisticsTest {

    private static final String APP_NAME_TEST = "Basic statistics Test";
    private EvaluateStatistics sut;
    private JavaSparkContext jsc;

    @Before
    public void setUp() {
        SparkConf conf = new SparkConf().setAppName(APP_NAME_TEST).setMaster("local[2]").set("spark.executor.memory", "2g");
        jsc = new JavaSparkContext(conf);
        sut = new EvaluateStatistics(jsc, "hdfs://U-PT793:54310/user/andreaferranti/dataset/HIGGS_275.csv");
    }

    @After
    public void tearDown() {
        jsc.stop();
    }

    @Test
    @Ignore
    public void testStatistics() {
        sut.getStatistics();
    }

}