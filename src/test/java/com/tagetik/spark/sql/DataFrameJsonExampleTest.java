package com.tagetik.spark.sql;

import org.apache.spark.sql.SparkSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DataFrameJsonExampleTest {

    private static final String APP_NAME = "Show information on Json file";
    private static final String DATA_PATH = "src/test/resources/people.json";
    private DataFrameJsonExample dataFrameJsonExample;
    private SparkSession spark;

    @Before
    public void setUp(){
        this.spark = SparkSession
                .builder()
                .appName(APP_NAME)
                .master("local[*]")
                .getOrCreate();
        dataFrameJsonExample = new DataFrameJsonExample(spark, DATA_PATH);
    }

    @After
    public void tearDown(){
        this.spark.stop();
    }

    @Test
    public void test(){
        dataFrameJsonExample.printSomeInformation();
    }

}
