package com.tagetik.spark.sql;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmployeeJdbcDAOTest {

    private static final String APP_NAME = "Demo DataFrame JDBC Test";
    private EmployeeJdbcDAO employeeDAO;
    private SparkSession spark;

    @Before
    public void setUp(){
        this.spark = SparkSession
                .builder()
                .master("local[*]")
                .appName(APP_NAME)
                .getOrCreate();
        this.employeeDAO = new EmployeeJdbcDAO(spark);
    }

    @After
    public void tearDown(){
        this.spark.stop();
    }

    @Test
    @Ignore
    public void test(){
        Dataset<Row> employeeName = this.employeeDAO.getEmployeeName();
        assertEquals("Andrea", employeeName.first().getString(0));
        employeeName.show();
    }

}
