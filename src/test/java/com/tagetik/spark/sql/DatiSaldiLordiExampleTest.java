package com.tagetik.spark.sql;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class DatiSaldiLordiExampleTest {
    private static final String APP_NAME = "Demo DataFrame JDBC Test";
    private DatiSaldiLordiExample employeeDAO;
    private SparkSession spark;

    @Before
    public void setUp(){
        this.spark = SparkSession
                .builder()
                .master("local[*]")
                .appName(APP_NAME)
                .getOrCreate();
        this.employeeDAO = new DatiSaldiLordiExample(spark);
    }

    @After
    public void tearDown(){
        this.spark.stop();
    }

    @Test
    @Ignore
    public void test(){
        Row row = employeeDAO.getFirstRow();
        System.out.println(row.toString());
    }
}